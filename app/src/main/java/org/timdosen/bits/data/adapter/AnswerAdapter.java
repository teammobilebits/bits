package org.timdosen.bits.data.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.timdosen.bits.R;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.models.Answer;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.fragments.ProfileFragment;
import org.timdosen.bits.fragments.QuestionDetailsFragment;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;
import org.timdosen.bits.utils.Utils;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by rynto14 on 5/7/2017.
 */

public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.AnswerViewHolder> {
    private Activity activity;
    private ArrayList<Answer> answers = new ArrayList<Answer>();
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    public static class AnswerViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        ImageView imgProfil;
        TextView lblUsername, lblAnswer, lblDate;
        LinearLayout userProfile;

        AnswerViewHolder(View v){
            super(v);
            cv = (CardView) v.findViewById(R.id.cvAnswerList);

            imgProfil = (ImageView) v.findViewById(R.id.imgProfil);
            lblUsername = (TextView) v.findViewById(R.id.lblUsername);
            lblAnswer = (TextView) v.findViewById(R.id.lblAnswer);
            lblDate = (TextView) v.findViewById(R.id.lblDate);
            userProfile = (LinearLayout) v.findViewById(R.id.userProfile);
        }
    }

    public AnswerAdapter(Activity activity, ArrayList<Answer> a){
        this.answers = a;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
    }

    @Override
    public int getItemCount() {
        if(answers == null) return 0;
        return answers.size();
    }

    @Override
    public AnswerAdapter.AnswerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_answer, parent, false);
        return new AnswerAdapter.AnswerViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AnswerAdapter.AnswerViewHolder holder, int position) {
        final Answer a = answers.get(position);

        imageLoader.DisplayImage(HttpRequest.baseURL + "user_img/"+a.getUserImg(), holder.imgProfil);
        holder.lblUsername.setText(a.getAnsweredby());
        holder.lblAnswer.setText(a.getAnswer());
        holder.lblDate.setText(a.getDate());

        // Get username form sqlite
        Account account = new Account(activity.getApplicationContext());
        final String username = account.getUsername();

        holder.imgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("username", a.getAnsweredby());
                if(username.equals(a.getAnsweredby())) bundle.putString("mode", "self");
                else bundle.putString("mode", "else");
                profileFragment.setArguments(bundle);
                new MainActivity().changeFragment(profileFragment, true, ProfileFragment.TAG);
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void removeAllItems(){
        answers = new ArrayList<Answer>();
        notifyDataSetChanged();
    }
}
