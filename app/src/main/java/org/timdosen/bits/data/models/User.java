package org.timdosen.bits.data.models;

/**
 * Created by rynto14 on 4/16/2017.
 */

public class User {
    private String username;
    private String password;
    private String confPassword;
    private String email;
    private String fullname;
    private String type;
    private String img;

    public User(){}
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }
    public User(String username, String img, String email, String fullname, String type) {
        this.username = username;
        this.img = img;
        this.email = email;
        this.fullname = fullname;
        this.type = type;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getConfPassword() {
        return confPassword;
    }

    public void setConfPassword(String confPassword) {
        this.confPassword = confPassword;
    }

}
