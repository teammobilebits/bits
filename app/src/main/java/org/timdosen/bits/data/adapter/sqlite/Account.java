package org.timdosen.bits.data.adapter.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import org.timdosen.bits.data.models.User;

/**
 * Created by rynto14 on 4/15/2017.
 */

public class Account extends SQLiteOpenHelper {
    public static final String TBL_NAME = "tbl_user";

    /*
    * QUERY UNTUK BIKIN DAN DROP TABLE
    * */
    public static final String CREATE_TABLE = String.format(
            "CREATE TABLE %s(%s, %s, %s, %s, %s, %s, %s, %s, %s)",
            TBL_NAME,
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "username VARCHAR(40)",
            "fullname VARCHAR(40)",
            "type VARCHAR(40)",
            "img VARCHAR(40)",
            "email VARCHAR(40)",
            "questionCount VARCHAR(10)",
            "answerCount VARCHAR(10)",
            "flags VARCHAR(10)"
    );
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TBL_NAME;

    public Account(Context context){
        super(context, InitDB.DATABASE_NAME, null, InitDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}

    public boolean isAppHaveUser(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM " + TBL_NAME, null);
        int ctr = c.getCount();
        c.close();

        db.close();
        if(ctr > 0) return true;
        return false;
    }

    public String getUsername(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM " + TBL_NAME, null);
        int ctr = c.getCount();
        c.moveToFirst();
        String username = c.getString(c.getColumnIndex("username"));
        c.close();

        db.close();
        if(ctr > 0) return username;
        return null;
    }
    public User getUser(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TBL_NAME, null);
        int ctr = c.getCount();
        c.moveToFirst();
        String username = c.getString(c.getColumnIndex("username"));
        String fullname = c.getString(c.getColumnIndex("fullname"));
        String email = c.getString(c.getColumnIndex("email"));
        String img = c.getString(c.getColumnIndex("img"));
        String type = c.getString(c.getColumnIndex("type"));
        c.close();
        db.close();

        return new User(username, img, email, fullname, type);
    }
    public void setProfile(String fullname, String email){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put("email", email);
        val.put("fullname", fullname);

        db.update(TBL_NAME, val, "id=1",null);
        db.close();
    }
    public void addAccount(String username, String fullname, String email, String type, String img, String questionCount, String answerCount){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put("username", username);
        val.put("fullname", fullname);
        val.put("email", email);
        val.put("type", type);
        val.put("img", img);
        val.put("questionCount", questionCount);
        val.put("answerCount", answerCount);
        val.put("flags", "perm");

        db.insert(TBL_NAME, null, val);
        db.close();
    }

    public void deleteAccount(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TBL_NAME);
        db.close();
    }

    public String getUserImage(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor c = db.rawQuery("SELECT img FROM " + TBL_NAME, null);
        int ctr = c.getCount();
        c.moveToFirst();
        String imgProfil = c.getString(c.getColumnIndex("img"));
        c.close();

        db.close();
        if(ctr > 0) return imgProfil;
        return null;
    }
}
