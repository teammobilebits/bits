package org.timdosen.bits.data.adapter;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.timdosen.bits.R;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by rynto14 on 4/24/2017.
 */

public class CustomListViewAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<Question> questions;
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    public CustomListViewAdapter(Activity activity, ArrayList<Question> questions){
        this.activity = activity;
        this.questions = questions;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(convertView == null) v = inflater.inflate(R.layout.list_custom_question, null);

        ImageView imgProfil = (ImageView) v.findViewById(R.id.imgProfil);
        TextView lblUsername = (TextView) v.findViewById(R.id.lblUsername);
        TextView lblAction = (TextView) v.findViewById(R.id.lblAction);
        TextView lblQuestion = (TextView) v.findViewById(R.id.lblQuestion);
        TextView lblView = (TextView) v.findViewById(R.id.lblViews);
        TextView lblAnswer = (TextView) v.findViewById(R.id.lblAnswer);

        Question q = questions.get(position);

        Log.d("usernaame", q.getAskedby());
        lblUsername.setText(q.getAskedby());
        lblAction.setText(q.getLastAction());
        lblQuestion.setText(q.getQuestion());
        lblView.setText(q.getView() + " views");
        lblAnswer.setText(q.getAnswer() + " Answer");
        imageLoader.DisplayImage(q.getImage(), imgProfil);


        return v;
    }
}
