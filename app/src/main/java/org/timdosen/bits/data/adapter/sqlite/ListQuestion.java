package org.timdosen.bits.data.adapter.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.timdosen.bits.data.models.Question;

import java.util.ArrayList;

/**
 * Created by rynto14 on 4/23/2017.
 */

public class ListQuestion extends SQLiteOpenHelper {
    public static final String TBL_NAME = "tbl_list_question";

    /*
    * QUERY UNTUK BIKIN DAN DROP TABLE
    * */
    public static final String CREATE_TABLE = String.format(
            "CREATE TABLE %s(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
            TBL_NAME,
            "id INTEGER PRIMARY KEY AUTOINCREMENT",
            "qid VARCHAR(5)",
            "askedby VARCHAR(150)",
            "question VARCHAR(150)",
            "view VARCHAR(5)",
            "answer VARCHAR(5)",
            "total_answer VARCHAR(5)",
            "tags VARCHAR(200)",
            "last_action VARCHAR(100)",
            "profpic VARCHAR(100)",
            "type VARCHAR(10)"
    );
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TBL_NAME;

    public ListQuestion(Context context){
        super(context, InitDB.DATABASE_NAME, null, InitDB.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {}

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}


    // Query
    public void addQuestion(Question question, String type){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues val = new ContentValues();
        val.put("qid", question.getId());
        val.put("question", question.getQuestion());
        val.put("view", question.getView());
        val.put("answer", question.getAnswer());
        val.put("total_answer", question.getTotalAnswer());
        val.put("tags", question.getTags());
        val.put("last_action", question.getLastAction());
        val.put("askedby", question.getAskedby());
        val.put("profpic", question.getImage());
        val.put("type", type);

        db.insert(TBL_NAME, null, val);
        db.close();
    }

    public ArrayList<Question> getQuestionList(String type){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Question> ret = new ArrayList<>();

        Cursor c = db.rawQuery("SELECT * FROM " + TBL_NAME + " WHERE type = '" + type + "'", null);
        if(c.moveToFirst()){
            do {
                Question q = new Question();
                q.setId(c.getString(c.getColumnIndex("qid")));
                q.setAskedby(c.getString(c.getColumnIndex("askedby")));
                q.setImage(c.getString(c.getColumnIndex("profpic")));
                q.setQuestion(c.getString(c.getColumnIndex("question")));
                q.setLastAction(c.getString(c.getColumnIndex("last_action")));
                q.setView(c.getString(c.getColumnIndex("view")));
                q.setTotalAnswer(c.getString(c.getColumnIndex("total_answer")));

                ret.add(q);
                //Log.d("SIZE", ""+ret.size());
            } while (c.moveToNext());

            c.close();
            db.close();
            return  ret;
        }
        else {
            c.close();
            db.close();
            return null;
        }
    }

    public void deleteAllQuestion(String type){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM "+TBL_NAME+" WHERE type = '"+type+"'");
        db.close();
    }
}
