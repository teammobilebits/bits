package org.timdosen.bits.data.adapter.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by rynto14 on 4/15/2017.
 */

public class InitDB extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "bits_db";

    public InitDB(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        this.getReadableDatabase();
    }

    // Bikin semua tabel yang dibutuhkan
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(Account.CREATE_TABLE);
        db.execSQL(ListQuestion.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(Account.DROP_TABLE);
        db.execSQL(ListQuestion.DROP_TABLE);

        onCreate(db);
    }
}
