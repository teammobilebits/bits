package org.timdosen.bits.data.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.timdosen.bits.R;
import org.timdosen.bits.activities.AddEditQuestionActivity;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.fragments.QuestionDetailsFragment;
import org.timdosen.bits.utils.ImageLoader;

import java.util.ArrayList;

/**
 * Created by rynto14 on 4/25/2017.
 */

public class MyQuestionListAdapter extends RecyclerView.Adapter<MyQuestionListAdapter.QuestionViewHolder> {
    private Activity activity;
    private ArrayList<Question> questions = new ArrayList<Question>();
    private static LayoutInflater inflater = null;

    public static class QuestionViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        TextView txtJudul, txtView, txtAnswer;

        QuestionViewHolder(View v){
            super(v);
            cv = (CardView) v.findViewById(R.id.cvMyQuestion);

            txtJudul = (TextView) v.findViewById(R.id.txtJudul);
            txtView = (TextView) v.findViewById(R.id.txtView);
            txtAnswer = (TextView) v.findViewById(R.id.txtTotalAnswer);
        }
    }

    public MyQuestionListAdapter(Activity activity, ArrayList<Question> q){
        this.questions = q;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        if(questions == null) return 0;
        System.out.println("UKURAQN " + questions.size());
        return questions.size();
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_my_question, parent, false);
        return new QuestionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        final Question q = questions.get(position);

        holder.txtJudul.setText(q.getQuestion());
        holder.txtAnswer.setText(q.getTotalAnswer() + " answer");
        holder.txtView.setText(q.getView() + " views");


        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CharSequence menu[] = new CharSequence[] {"Edit this Question", "View this question"};

                AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                builder.setTitle("Choose an Action");
                builder.setItems(menu, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which == 0){
                            Intent i = new Intent(activity, AddEditQuestionActivity.class);
                            i.putExtra("mode", "edit");
                            i.putExtra("id", q.getId());
                            activity.startActivity(i);
                        }
                        else{
                            QuestionDetailsFragment questionDetailsFragment = new QuestionDetailsFragment();
                            Bundle bundle = new Bundle();
                            bundle.putString("question_id", q.getId());
                            questionDetailsFragment.setArguments(bundle);
                            new MainActivity().changeFragment(questionDetailsFragment, true, QuestionDetailsFragment.TAG);
                        }
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void removeAllItems(){
        questions = new ArrayList<Question>();
        notifyDataSetChanged();
    }
}
