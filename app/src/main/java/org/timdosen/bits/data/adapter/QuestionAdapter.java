package org.timdosen.bits.data.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.timdosen.bits.R;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.fragments.QuestionDetailsFragment;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;
import org.timdosen.bits.utils.Utils;

import java.util.ArrayList;

/**
 * Created by rynto14 on 4/25/2017.
 */

public class QuestionAdapter extends RecyclerView.Adapter<QuestionAdapter.QuestionViewHolder> {
    private Activity activity;
    private ArrayList<Question> questions = new ArrayList<Question>();
    private static LayoutInflater inflater = null;
    public ImageLoader imageLoader;

    public static class QuestionViewHolder extends RecyclerView.ViewHolder{
        CardView cv;
        ImageView imgProfil;
        TextView lblUsername, lblAction, lblQuestion, lblView, lblAnswer;

        QuestionViewHolder(View v){
            super(v);
            cv = (CardView) v.findViewById(R.id.cvQuestion);

            imgProfil = (ImageView) v.findViewById(R.id.imgProfil);
            lblUsername = (TextView) v.findViewById(R.id.lblUsername);
            lblAction = (TextView) v.findViewById(R.id.lblAction);
            lblQuestion = (TextView) v.findViewById(R.id.lblQuestion);
            lblView = (TextView) v.findViewById(R.id.lblViews);
            lblAnswer = (TextView) v.findViewById(R.id.lblAnswer);
        }
    }

    public QuestionAdapter(Activity activity, ArrayList<Question> q){
        this.questions = q;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(activity.getApplicationContext());
    }

    @Override
    public int getItemCount() {
        if(questions == null) return 0;
        return questions.size();
    }

    @Override
    public QuestionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_question, parent, false);
        return new QuestionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(QuestionViewHolder holder, int position) {
        final Question q = questions.get(position);

        imageLoader.DisplayImage(HttpRequest.baseURL + "user_img/"+q.getImage(), holder.imgProfil);
        holder.lblAction.setText(q.getLastAction());
        holder.lblQuestion.setText(q.getQuestion());
        holder.lblUsername.setText(q.getAskedby());
        holder.lblAnswer.setText(q.getTotalAnswer() + " answer");
        holder.lblView.setText(q.getView() + " views");


        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionDetailsFragment questionDetailsFragment = new QuestionDetailsFragment();
                Bundle bundle = new Bundle();
                Log.d("asd", q.getId());
                bundle.putString("question_id", q.getId());
                questionDetailsFragment.setArguments(bundle);
                new MainActivity().changeFragment(questionDetailsFragment, true, QuestionDetailsFragment.TAG);
                //Toast.makeText(v.getContext(), q.getLastAction(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public void removeAllItems(){
        questions = new ArrayList<Question>();
        notifyDataSetChanged();
    }
}
