package org.timdosen.bits.data.models;

/**
 * Created by rynto14 on 5/7/2017.
 */

public class Answer {
    private String answer;
    private String date;
    private String answeredby;
    private String userImg;
    private String answerID;

    public Answer(String answer, String date, String answeredby, String userImg, String answerID) {
        this.answer = answer;
        this.date = date;
        this.answeredby= answeredby;
        this.userImg = userImg;
        this.answerID = answerID;
    }


    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getAnsweredby() {
        return answeredby;
    }

    public void setAnsweredby(String answeredby) {
        this.answeredby = answeredby;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAnswerID() {
        return answerID;
    }

    public void setAnswerID(String answerID) {
        this.answerID = answerID;
    }
}
