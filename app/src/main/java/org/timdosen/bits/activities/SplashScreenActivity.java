package org.timdosen.bits.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.adapter.sqlite.InitDB;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Start main process
        new AppsInitializationWorks().execute("");
    }

    private class AppsInitializationWorks extends AsyncTask<String, Void, Void>{
        private boolean isUserLogin = false;

        @Override
        protected Void doInBackground(String... params) {
            // Init Database;
            new InitDB(getApplicationContext());
            // cek uda login belom
            Account user = new Account(getApplicationContext());
            isUserLogin = user.isAppHaveUser();

            if(isUserLogin){
                // load data dari server
                try{
                    Thread.sleep(2000);
                }catch (Exception e){
                    e.printStackTrace();
                }
                // masuk ke menu utama
                Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
            else{
                // panggil activity login
                startActivity(new Intent(SplashScreenActivity.this, LoginActivity.class));
                finish();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void v) {
        }
    }
}
