package org.timdosen.bits.activities;

import android.app.SearchManager;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.fragments.MyQuestionFragment;
import org.timdosen.bits.fragments.ProfileFragment;
import org.timdosen.bits.fragments.SearchResultFragment;
import org.timdosen.bits.fragments.TabFragment;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    // Define STATE
    public static final int MAIN = 1;
    public static final int PROFILE = 2;
    public static final int ADD_QUESTION = 3;
    public static final int QUESTION_DETAIL = 4;
    public static final int UPDATE = 5;

    public static int currentState = 0;

    private GoogleApiClient mGoogleApiClient;

    private static FragmentManager fragmentManager;

    private FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //new getQuestionFeed().execute();
        currentState = MainActivity.MAIN;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddEditQuestionActivity.class);
                i.putExtra("mode", "add");
                startActivity(i);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final View headerView = navigationView.getHeaderView(0);
        Account user = new Account(getApplicationContext());
        // Setup image
        ImageView imgProfil = (ImageView) headerView.findViewById(R.id.imgProfil);
        ImageLoader imageLoader = new ImageLoader(getApplicationContext());
        imageLoader.DisplayImage(HttpRequest.baseURL + "user_img/"+user.getUserImage(), imgProfil);

        //username
        TextView username = (TextView) headerView.findViewById(R.id.txtUsername);
        username.setText(user.getUsername());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);

        SearchView.OnQueryTextListener textListener = new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.equals("")) return false;
                else{
                    Bundle bundle = new Bundle();
                    bundle.putString("query", query);
                    SearchResultFragment fragobj = new SearchResultFragment();
                    fragobj.setArguments(bundle);

                    changeFragment(fragobj, true, SearchResultFragment.TAG);
                    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                    drawer.closeDrawer(GravityCompat.START);

                    getWindow().setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
                    );
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        };
        searchView.setOnQueryTextListener(textListener);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        super.onStart();
        switch (currentState){
            case MAIN:{
                changeFragment(new TabFragment(), false, "");
                break;
            }
        }

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            changeFragment(new TabFragment(), true, TabFragment.TAG);
        }
        else if (id == R.id.nav_profile) {
            Account a = new Account(getApplicationContext());
            ProfileFragment profileFragment = new ProfileFragment();
            Bundle bundle = new Bundle();
            bundle.putString("username", a.getUsername());
            bundle.putString("mode", "self");
            profileFragment.setArguments(bundle);
            changeFragment(profileFragment, true, ProfileFragment.TAG);
        }
        else if(id == R.id.nav_my_question){
            changeFragment(new MyQuestionFragment(), true, MyQuestionFragment.TAG);
        }
        else if(id == R.id.nav_logout){
            userLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public static void changeFragment(Fragment fragment, boolean backTrack, String tag){
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_fragment, fragment);
        if(backTrack) fragmentTransaction.addToBackStack(tag);
        fragmentTransaction.commit();
    }

    private void userLogout(){
        // Log out facebook
        LoginManager.getInstance().logOut();

        // Google logout
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
            new ResultCallback<Status>() {
                @Override
                public void onResult(Status status) { }
            }
        );

        // Delete from SQLite DB
        new Account(getApplicationContext()).deleteAccount();

        startActivity(new Intent(MainActivity.this, LoginActivity.class));
    }
}
