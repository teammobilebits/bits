package org.timdosen.bits.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.models.User;
import org.timdosen.bits.utils.HttpRequest;

public class RegisterActivity extends AppCompatActivity {
    private ProgressDialog regisLoading = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Tambah back button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        final EditText txtFullname = (EditText) findViewById(R.id.txtFullname);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);
        final EditText txtConfPass = (EditText) findViewById(R.id.txtConfPass);

        // Get Extras if exists
        Bundle extras = getIntent().getExtras();
        if(extras != null){
            txtEmail.setText(extras.getString("email"));
            txtFullname.setText(extras.getString("fullname"));
        }

        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtUsername.getText().toString();
                String email = txtEmail.getText().toString();
                String fullname = txtFullname.getText().toString();
                String password = txtPassword.getText().toString();
                String confPass = txtConfPass.getText().toString();

                if(username.equals("")) txtUsername.setError("Username is required");
                else if(email.equals("")) txtEmail.setError("E-mail is required");
                else if(fullname.equals("")) txtFullname.setError("Fullname is required");
                else if(password.equals("")) txtPassword.setError("Password is required");
                else if(confPass.equals("")) txtConfPass.setError("Password confirmation is required");
                else{
                    if(!password.equals(confPass)){
                        txtConfPass.setError("Invalid password combination");;
                        txtConfPass.setText("");
                    }
                    else if(!cekFormatEmail(email)){
                        txtEmail.setError("Invalid E-mail format");
                        txtEmail.setText("");
                    }
                    else{
                        regisLoading = new ProgressDialog(RegisterActivity.this);
                        regisLoading.setIndeterminate(true);
                        regisLoading.setCanceledOnTouchOutside(false);
                        regisLoading.setMessage("Authenticating...");
                        regisLoading.show();

                        User u = new User(username, password);
                        u.setEmail(email);
                        u.setFullname(fullname);
                        u.setConfPassword(confPass);
                        new doRegister().execute(u);
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private boolean cekFormatEmail(String email){
        return true;
    }
    private class doRegister extends AsyncTask<User, Void, JSONObject>{
        User user = null;
        @Override
        protected JSONObject doInBackground(User... params) {
            user = params[0];

            HttpRequest userLogin = new HttpRequest(HttpRequest.baseURL + "user/register", "POST");
            userLogin.addParam("username", user.getUsername());
            userLogin.addParam("fullname", user.getFullname());
            userLogin.addParam("password", user.getPassword());
            userLogin.addParam("confPass", user.getConfPassword());
            userLogin.addParam("email", user.getEmail());

            try {
                String res = userLogin.execute();
                JSONObject loginObj = new JSONObject(res);
                return loginObj;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("payload"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    Thread thread = new Thread(){
                        @Override
                        public void run() {
                            try{
                                HttpRequest httpRequest = new HttpRequest(HttpRequest.baseURL+"user/login/facebook", "POST");
                                httpRequest.addParam("email", user.getEmail());
                                String res = httpRequest.execute();

                                JSONObject obj = new JSONObject(res);
                                if(obj.getBoolean("success")){
                                    JSONObject userData = obj.getJSONObject("payload");
                                    // Masukin datanya ke local database. Jadi nanti dimana mana tinggal panggil di db, ga usah kirim"
                                    Account user = new Account(getApplicationContext());
                                    user.addAccount(
                                            userData.getString("username"),
                                            userData.getString("fullname"),
                                            userData.getString("email"),
                                            userData.getString("type"),
                                            userData.getString("img"),
                                            userData.getString("questionAsked"),
                                            userData.getString("answerCount")
                                    );

                                    // Balik lagi ke splash screen
                                    startActivity(new Intent(RegisterActivity.this, SplashScreenActivity.class));
                                }
                                finish();
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    };
                    thread.start();

                    //LoginActivity.doLogin.execute((Runnable) new User(user.getUsername(), user.getPassword()));
                }
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                regisLoading.dismiss();
                                Toast.makeText(getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
