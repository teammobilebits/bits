package org.timdosen.bits.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.utils.HttpRequest;

public class PostAnswerActivity extends AppCompatActivity {
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_answer);

        // Setting loading dialog
        // Show loading dialog
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");

        final String questionID = getIntent().getExtras().getString("id");

        final EditText txtAnswer = (EditText) findViewById(R.id.txtAnswer);
        Button btnSubmit = (Button) findViewById(R.id.btn_Submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                Account account = new Account(getApplicationContext());
                String username = account.getUsername();
                new PostAnswer().execute(questionID, username, txtAnswer.getText().toString());
            }
        });

        Button btnCancel = (Button) findViewById(R.id.btn_Cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private class PostAnswer extends AsyncTask<String, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(String... params) {
            String questionID = params[0];
            String username = params[1];
            String answer = params[2];

            HttpRequest getQuestionDetails = new HttpRequest(HttpRequest.baseURL + "answer/add", "POST");
            try{
                System.out.println(questionID + " " + username + " " + answer);
                getQuestionDetails.addParam("qid", questionID);
                getQuestionDetails.addParam("username", username);
                getQuestionDetails.addParam("answer", answer);
                String res = getQuestionDetails.execute();
                JSONObject getQuestObj = new JSONObject(res);
                return getQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try{
                loadingDialog.hide();
                if(jsonObject.getBoolean("success")){
                    finish();
                    System.out.println("SUKSES");
                    loadingDialog.dismiss();
                }
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(getApplicationContext(), jsonObject.getJSONObject("errMsg").getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
