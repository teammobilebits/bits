package org.timdosen.bits.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.models.User;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.utils.HttpRequest;

import com.facebook.FacebookSdk;

import java.security.MessageDigest;
import java.util.Arrays;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    ProgressDialog loginLoading = null;
    CallbackManager callbackManager = null;

    // Facebook login button
    Button btnLoginFacebook = null;
    LoginButton facebookLogin = null;

    // Google login button
    Button btnLoginGoogle = null;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Initialize callback manager
        callbackManager = CallbackManager.Factory.create();

        // Initialize loading dialog
        loginLoading = new ProgressDialog(LoginActivity.this);
        loginLoading.setIndeterminate(true);
        loginLoading.setCanceledOnTouchOutside(false);
        loginLoading.setMessage("Authenticating...");

        final TextView lblForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        lblForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class));
            }
        });
        final EditText txtUsername = (EditText) findViewById(R.id.txtUsername);
        final EditText txtPassword = (EditText) findViewById(R.id.txtPassword);
        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();

                if (username.equals("")) txtUsername.setError("Username is required");
                else if (password.equals("")) txtPassword.setError("Password is required");
                else {
                    loginLoading.show();

                    User u = new User(txtUsername.getText().toString(), txtPassword.getText().toString());
                    new doLogin().execute(u);
                }

            }
        });

        TextView txtSignUp = (TextView) findViewById(R.id.txtSignUp);
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                finish();
            }
        });


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

        /**
        * HANDLING ALL FACEBOOK API
        * */
        btnLoginFacebook = (Button) findViewById(R.id.btnLoginFacebook);
        facebookLogin = (LoginButton) findViewById(R.id.facebook_login_button);

        List<String> facebookPermissionNeeds = Arrays.asList("email", "public_profile");
        facebookLogin.setReadPermissions(facebookPermissionNeeds);
        facebookLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback(){
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        final String name, email;
                        try{
                            name = object.getString("name");
                            email = object.getString("email");

                            Log.d("fullname email", name + " " + email);
                            thirdPartyLogin(email, name, "facebook");
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });

                Bundle param = new Bundle();
                param.putString("fields", "name, email");
                graphRequest.setParameters(param);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {}
            @Override
            public void onError(FacebookException error) {
                Log.d("Error", error.getMessage());
            }
        });


        /**
        * HANDLING GOOGLE LOGIN API
        * */
        btnLoginGoogle = (Button) findViewById(R.id.btnLoginGoogle);

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        Log.d("GOOGLE SIGNIN", "onConnectionFailed:" + connectionResult);
                    }
                })
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addApi(AppIndex.API)
                .build();
        btnLoginGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("click", "true");
                loginLoading.show();
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });
    }

    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Login Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mGoogleApiClient.connect();
        AppIndex.AppIndexApi.start(mGoogleApiClient, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mGoogleApiClient, getIndexApiAction());
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            Log.d("GOOGLE SIGNIN", "handleSignInResult:" + result.isSuccess());
            if (result.isSuccess()) {
                // Signed in successfully, show authenticated UI.
                GoogleSignInAccount acct = result.getSignInAccount();

                String personName = acct.getDisplayName();
                String email = acct.getEmail();

                thirdPartyLogin(email, personName, "google");
            }
        }
    }

    public void onClick(View v){
        if(v == btnLoginFacebook){
            loginLoading.show();
            facebookLogin.performClick();
        }
    }

    private void thirdPartyLogin(final String email, final String fullname, final String party){
        Thread thread = new Thread(){
            @Override
            public void run() {
                try{
                    HttpRequest httpRequest = new HttpRequest(HttpRequest.baseURL+"user/login/"+party, "POST");
                    httpRequest.addParam("email", email);;
                    String res = httpRequest.execute();

                    JSONObject obj = new JSONObject(res);
                    if(obj.getBoolean("success")){
                        JSONObject userData = obj.getJSONObject("payload");
                        // Masukin datanya ke local database. Jadi nanti dimana mana tinggal panggil di db, ga usah kirim"
                        Account user = new Account(getApplicationContext());
                        user.addAccount(
                                userData.getString("username"),
                                userData.getString("fullname"),
                                userData.getString("email"),
                                userData.getString("type"),
                                userData.getString("img"),
                                userData.getString("questionAsked"),
                                userData.getString("answerCount")
                        );

                        // Balik lagi ke splash screen
                        startActivity(new Intent(LoginActivity.this, SplashScreenActivity.class));
                    }
                    else{
                        // Data nya gaada, direct ke halaman regis
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        intent.putExtra("fullname", fullname);
                        intent.putExtra("email", email);
                        startActivity(intent);
                    }
                    finish();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    public class doLogin extends AsyncTask<User, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(User... params){
            User user = params[0];

            HttpRequest userLogin = new HttpRequest(HttpRequest.baseURL + "user/login", "POST");
            userLogin.addParam("username", user.getUsername());
            userLogin.addParam("password", user.getPassword());

            try {
                String res = userLogin.execute();
                JSONObject loginObj = new JSONObject(res);
                return loginObj;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try {
                boolean isSuccess = jsonObject.getBoolean("success");

                loginLoading.dismiss();
                if (isSuccess) {
                    JSONObject userData = jsonObject.getJSONObject("payload");
                    // Masukin datanya ke local database. Jadi nanti dimana mana tinggal panggil di db, ga usah kirim"
                    Account user = new Account(getApplicationContext());
                    user.addAccount(
                            userData.getString("username"),
                            userData.getString("fullname"),
                            userData.getString("email"),
                            userData.getString("type"),
                            userData.getString("img"),
                            userData.getString("questionAsked"),
                            userData.getString("answerCount")
                    );

                    // Balik lagi ke splash screen
                    startActivity(new Intent(LoginActivity.this, SplashScreenActivity.class));
                    finish();
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Toast.makeText(getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

