package org.timdosen.bits.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.utils.HttpRequest;

public class AddEditQuestionActivity extends AppCompatActivity {
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_questionn);

        final String mode = getIntent().getExtras().getString("mode");

        // Show loading dialog
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");

        if(mode.equals("edit")){
            loadingDialog.show();

            new GetQuestionDetail().execute(getIntent().getExtras().getString("id"));
        }

        Button btnSubmit = (Button) findViewById(R.id.btn_Submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText txtQuestion = (EditText) findViewById(R.id.txt_Question);
                final EditText txtTags = (EditText) findViewById(R.id.txt_Tags);
                final EditText txtQDetails = (EditText) findViewById(R.id.txt_QuestionDetails);

                String question = txtQuestion.getText().toString();
                String tags = txtTags.getText().toString();
                String questionDetails = txtQDetails.getText().toString();

                if(question.equals("")) txtQuestion.setError("Question is Reuired");
                else if(tags.equals("")) txtTags.setError("Tags is Required");
                else if(questionDetails.equals("")) txtQDetails.setError("Question Details is Required");
                else{
                    if(mode.equals("add")){
                        Question q = new Question(question, tags, questionDetails);
                        new doAddQuestion().execute(q);
                    }
                    else{
                        loadingDialog.show();
                        new doEditQuestion().execute(getIntent().getExtras().getString("id"), question, tags, questionDetails);
                    }
                }

            }
        });

        Button btnCancel = (Button) findViewById(R.id.btn_Cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private class doAddQuestion extends AsyncTask<Question, Void, JSONObject>{
        Question question = null;

        @Override
        protected JSONObject doInBackground(Question... params) {
            question = params[0];
            Account acc = new Account(getApplicationContext());
            String username = acc.getUsername();


            HttpRequest addQuestion = new HttpRequest(HttpRequest.baseURL + "question/add", "POST");
            addQuestion.addParam("question", question.getQuestion());
            addQuestion.addParam("tags", question.getTags());
            addQuestion.addParam("details", question.getQuestionDetails());
            if(!username.equals(null)){
                addQuestion.addParam("username", username);
            }

            try{
                String res = addQuestion.execute();
                JSONObject addQuestObj = new JSONObject(res);
                return addQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                Toast.makeText(getApplicationContext(),"Success", Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    finish();
                }
                else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                Toast.makeText(getApplicationContext(),jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }


    }

    private class GetQuestionDetail extends AsyncTask<String, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try{
                String qid = params[0];
                HttpRequest getQuestionDetails = new HttpRequest(HttpRequest.baseURL + "question/id/" + qid + "/false", "GET");
                String res = getQuestionDetails.execute();
                jsonObject = new JSONObject(res);
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                return jsonObject;
            }
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    final JSONObject obj = jsonObject.getJSONObject("payload");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                EditText txtQuestion = (EditText) findViewById(R.id.txt_Question);
                                EditText txtTags = (EditText) findViewById(R.id.txt_Tags);
                                EditText txtQDetails = (EditText) findViewById(R.id.txt_QuestionDetails);

                                txtQuestion.setText(obj.getString("title"));
                                txtQDetails.setText(obj.getString("content"));

                                JSONArray tags = obj.getJSONArray("tags");
                                int len = tags.length();
                                String strTag = "";
                                for(int i=0; i<len; i++){
                                    JSONObject jObj = tags.getJSONObject(i);
                                    if(i > 0) strTag += ",";
                                    strTag += jObj.getString("name");
                                }
                                txtTags.setText(strTag);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                Toast.makeText(getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                loadingDialog.hide();
            }
        }
    }

    private class doEditQuestion extends AsyncTask<String, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try{
                String qid = params[0];
                String question = params[1];
                String tags = params[2];
                String details = params[3];

                HttpRequest updateQuestion = new HttpRequest(HttpRequest.baseURL + "question/update", "POST");
                updateQuestion.addParam("id", qid);
                updateQuestion.addParam("question", question);
                updateQuestion.addParam("tags", tags);
                updateQuestion.addParam("details", details);

                String res = updateQuestion.execute();
                return new JSONObject(res);

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                return jsonObject;
            }
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try {
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess) finish();
                else{
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{Toast.makeText(getApplicationContext(),jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();} catch (Exception e){e.printStackTrace();}
                        }
                    });
                }

            } catch (Exception e){
                e.printStackTrace();
            } finally {
                loadingDialog.hide();
            }
        }
    }
}
