package org.timdosen.bits.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.timdosen.bits.R;

public class ForgotPasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        final EditText txtEmail = (EditText) findViewById(R.id.txtEmail);
        Button btnRequestPw = (Button) findViewById(R.id.btnRequestNewPw);
        btnRequestPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = txtEmail.getText().toString();
                if(email.equals("")){
                    txtEmail.setError("Email is reuired");
                }else{
                    //ini gatau mau ngapain
                }
            }
        });
    }
}
