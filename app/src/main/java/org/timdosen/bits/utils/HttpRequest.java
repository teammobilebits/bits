package org.timdosen.bits.utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rynto14 on 4/15/2017.
 */

public class HttpRequest {
    private Map<String, String> params;
    private String method;
    private String url;
    //public static String baseURL = "http://developer.rudii14.xyz/v2/";
    public static String baseURL = "http://192.168.137.1/bitsAPI_2/";

    public HttpRequest(String url, String method){
        params = new HashMap<String, String>();

        this.method = method;
        this.url = url;
    }

    public void addParam(String key, String value){
        params.put(key, value);
    }

    private String generateParams(){
        String ret = "";
        boolean first = false;

        for (Map.Entry<String, String> pair : params.entrySet()) {
            if(!first) first = true;
            else ret += "&";

            ret +=  pair.getKey() + "=" + pair.getValue();
        }

        return ret;
    }

    public String execute() throws Exception{
        HttpURLConnection httpURLConnection = null;
        URL oUrl = null;
        String encodedParams = generateParams();

        if(method == "GET"){
            String finalUrl = this.url + "?" + encodedParams;
            oUrl = new URL(finalUrl);
            httpURLConnection = (HttpURLConnection) oUrl.openConnection();
            httpURLConnection.setRequestMethod("GET");
        }
        else if(method == "POST"){
            oUrl = new URL(url);
            httpURLConnection = (HttpURLConnection) oUrl.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(httpURLConnection.getOutputStream());
            wr.writeBytes(encodedParams);
            wr.flush();
            wr.close();
        }

        BufferedReader in = new BufferedReader(
                new InputStreamReader(httpURLConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}

