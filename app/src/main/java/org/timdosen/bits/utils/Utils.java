package org.timdosen.bits.utils;

/**
 * Created by rynto14 on 4/24/2017.
 */

import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    public static String formatDate(String mysqlDate){
        SimpleDateFormat df1 = new SimpleDateFormat("yyyy-MM-DD");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MM yyyy");

        String ret = null;
        try{
            ret = outputFormat.format(df1.parse(mysqlDate));
        } catch (Exception e){
            e.printStackTrace();
        }

        return ret;
    }
}
