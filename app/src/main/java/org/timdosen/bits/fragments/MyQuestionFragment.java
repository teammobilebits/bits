package org.timdosen.bits.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.AnswerAdapter;
import org.timdosen.bits.data.adapter.MyQuestionListAdapter;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.models.Answer;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.Utils;

import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by rynto14 on 5/16/2017.
 */

public class MyQuestionFragment extends Fragment {
    private Activity parentActivity;
    private ProgressDialog loadingDialog;
    private static RecyclerView rv;
    public static String TAG = "MY_QUESTION_LIST";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_question, container, false);
        parentActivity = getActivity();
        rv = (RecyclerView) rootView.findViewById(R.id.rvMyQuestionList);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        // Show loading dialog
        loadingDialog = new ProgressDialog(parentActivity);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");
        loadingDialog.show();

        new FetchMyQuestion().execute();
        return rootView;
    }

    private class FetchMyQuestion extends AsyncTask<Void, Void, JSONObject>{
        @Override
        protected JSONObject doInBackground(Void... params) {
            JSONObject jsonObject = null;
            try{
                HttpRequest getUserQuestionList = new HttpRequest(HttpRequest.baseURL + "question/userquestion", "GET");
                getUserQuestionList.addParam("username", new Account(parentActivity.getApplicationContext()).getUsername());

                String res = getUserQuestionList.execute();
                //System.out.println(res);
                jsonObject = new JSONObject(res);
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                return jsonObject;
            }
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    JSONArray jsonArray = jsonObject.getJSONArray("payload");
                    int len = jsonArray.length();
                    System.out.println("PANJANG DATA: " + len);
                    final ArrayList<Question> listQuestion = new ArrayList<>();

                    for(int i=0; i<len; i++){
                        System.out.println("LOOP: " + i);
                        JSONObject obj = jsonArray.getJSONObject(i);
                        Question q = new Question();
                        q.setId(obj.getString("id"));
                        q.setQuestion(obj.getString("title"));
                        q.setView(obj.getString("view"));
                        q.setTotalAnswer(obj.getString("answer"));

                        listQuestion.add(q);
                    }
                    System.out.println(listQuestion.size());

                    // do adapter shit here
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                MyQuestionListAdapter adapter = new MyQuestionListAdapter(parentActivity, listQuestion);
                                rv.setAdapter(adapter);
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    loadingDialog.dismiss();
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                loadingDialog.hide();
            }
        }
    }
}
