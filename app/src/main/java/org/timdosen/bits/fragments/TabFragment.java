package org.timdosen.bits.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TabHost;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.activities.LoginActivity;
import org.timdosen.bits.data.adapter.sqlite.ListQuestion;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.fragments.tabchild.RecentQuestionTab;
import org.timdosen.bits.fragments.tabchild.TrendingQuestionTab;
import org.timdosen.bits.utils.HttpRequest;

import java.util.HashMap;
import java.util.List;

/**
 * Created by rynto14 on 4/23/2017.
 */

public class TabFragment extends Fragment {
    public static String TAG = "TAB";

    ExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    Activity parentActivity = null;
    ProgressDialog loadingQuestion = null;
    int currentTab = -1;

    private FragmentTabHost tabHost = null;

    public TabFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if(tabHost != null){
            tabHost.clearFocus();
            tabHost.clearAllTabs();
        }

        tabHost = new FragmentTabHost(getActivity());
        tabHost.setup(getActivity(), getChildFragmentManager(), R.layout.fragment_tab);
        tabHost.addTab(tabHost.newTabSpec("Tab1").setIndicator("Recent Question"),
                RecentQuestionTab.class, null);
        tabHost.addTab(tabHost.newTabSpec("Tab2").setIndicator("Trending Question"),
                TrendingQuestionTab.class, null);
        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                View currentView = tabHost.getCurrentView();
                if(tabHost.getCurrentTab() > currentTab) currentView.setAnimation(swipeAnimation(0));
                else currentView.setAnimation(swipeAnimation(1));
                currentTab = tabHost.getCurrentTab();
            }
        });

        parentActivity = getActivity();

        try{Thread.sleep(1500);} catch (Exception e){e.printStackTrace();}

        loadingQuestion = new ProgressDialog(parentActivity);
        loadingQuestion.setIndeterminate(true);
        loadingQuestion.setCanceledOnTouchOutside(false);
        loadingQuestion.setMessage("Loading data...");
        loadingQuestion.show();

        // Show button
        FloatingActionButton fab = (FloatingActionButton) parentActivity.findViewById(R.id.fab);
        fab.show();

        new getRecentQuestionFeed().execute();
        return tabHost;
    }

    private Animation swipeAnimation(int dir){
        // right = 0, left = 1
        float geser = 1.0f;
        Animation animation = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, (dir==0 ? +geser : -geser),
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f
        );
        animation.setDuration(800);
        animation.setInterpolator(new AccelerateInterpolator());
        return animation;
    }

    private class getRecentQuestionFeed extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            HttpRequest getQuestion = new HttpRequest(HttpRequest.baseURL + "question/recent", "GET");
            try{
                String res = getQuestion.execute();
                System.out.println(res);
                JSONObject getQuestObj = new JSONObject(res);
                return getQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    JSONArray payload = jsonObject.getJSONArray("payload");
                    int len = payload.length();
                    ListQuestion user = new ListQuestion(parentActivity.getApplicationContext());
                    user.deleteAllQuestion("RECENT");
                    for(int i=0; i<len; i++){
                        JSONObject obj = payload.getJSONObject(i);

                        Question q = new Question();
                        q.setId(obj.getString("id"));
                        q.setQuestion(obj.getString("title"));
                        q.setView(obj.getString("view"));
                        q.setAnswer(obj.getString("answer"));
                        q.setTotalAnswer(obj.getString("total_answer"));
                        q.setTags(obj.getString("tags"));
                        q.setAskedby(obj.getJSONObject("userdata").getString("username"));
                        q.setImage(obj.getJSONObject("userdata").getString("profpic"));
                        q.setLastAction(obj.getString("action"));

                        user.addQuestion(q, "RECENT");
                    }

                    new getTrendingQuestionFeed().execute();
                    RecentQuestionTab.updateListView();
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingQuestion.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(),jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

    }

    private class getTrendingQuestionFeed extends AsyncTask<Void, Void, JSONObject> {
        @Override
        protected JSONObject doInBackground(Void... params) {
            HttpRequest getQuestion = new HttpRequest(HttpRequest.baseURL + "question/trending", "GET");
            try{
                String res = getQuestion.execute();
                JSONObject getQuestObj = new JSONObject(res);
                return getQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    JSONArray payload = jsonObject.getJSONArray("payload");
                    int len = payload.length();
                    ListQuestion user = new ListQuestion(parentActivity.getApplicationContext());
                    user.deleteAllQuestion("TREND");
                    for(int i=0; i<len; i++){
                        Log.d("DEBUG", ""+i);
                        JSONObject obj = payload.getJSONObject(i);
                        Question q = new Question();
                        q.setId(obj.getString("id"));
                        q.setQuestion(obj.getString("title"));
                        q.setView(obj.getString("view"));
                        q.setAnswer(obj.getString("answer"));
                        q.setTotalAnswer(obj.getString("total_answer"));
                        q.setTags(obj.getString("tags"));
                        q.setAskedby(obj.getJSONObject("userdata").getString("username"));
                        q.setImage(obj.getJSONObject("userdata").getString("profpic"));
                        q.setLastAction(obj.getString("action"));

                        Log.d("DEBUG", obj.getString("title"));
                        user.addQuestion(q, "TREND");
                    }
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                Toast.makeText(parentActivity.getApplicationContext(),jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
                loadingQuestion.dismiss();
            }catch (Exception e){
                e.printStackTrace();
            }

            Log.d("DEBUG", "TRENDING UPDATE");
            RecentQuestionTab.updateListView();
            Log.d("DEBUG", "TRENDING DONE");
        }

    }
}
