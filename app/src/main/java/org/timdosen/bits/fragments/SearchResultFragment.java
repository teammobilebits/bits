package org.timdosen.bits.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.QuestionAdapter;
import org.timdosen.bits.data.models.Question;
import org.timdosen.bits.utils.HttpRequest;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rynto14 on 5/16/2017.
 */

public class SearchResultFragment extends Fragment {
    private Activity parentActivity;
    private ProgressDialog loadingDialog;
    private static RecyclerView rv;
    public static String TAG = "SEARCH_RESULT";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search_result, container, false);
        parentActivity = getActivity();
        rv = (RecyclerView) rootView.findViewById(R.id.rvSearchResult);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        // hide button
        FloatingActionButton fab = (FloatingActionButton) parentActivity.findViewById(R.id.fab);
        fab.hide();

        // Show loading dialog
        loadingDialog = new ProgressDialog(parentActivity);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");
        loadingDialog.show();

        // get query stringnyaa
        String query = this.getArguments().getString("query");

        // fetch the dataaaaaa
        new FetchSearchData().execute(query, "1");

        return rootView;
    }

    private String getPosition(String page, int batas){
        if(page == null) return "0";
        return "" + ((Integer.parseInt(page)-1) * batas);
    }

    private void initPaging(final String query, int len, final int curPage, int batas){
        ImageButton btnNext = (ImageButton) parentActivity.findViewById(R.id.btnNext);
        ImageButton btnPrev = (ImageButton) parentActivity.findViewById(R.id.btnPrev);
        final Spinner spnPaging = (Spinner) parentActivity.findViewById(R.id.spnPaging);

        // Hitung jumlah halaman
        int jumlahHalaman = (int) Math.ceil((double) len / batas);

        // Generate jumlah halaman dan msukin ke spinner, masukin ke adapter, lalu tempel ke spinner
        List<String> spinnerArray =  new ArrayList<String>();
        for(int i=1; i<=jumlahHalaman; i++) spinnerArray.add(""+i);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(parentActivity, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPaging.setAdapter(adapter);
        spnPaging.setSelection(adapter.getPosition(""+curPage));
        spnPaging.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String page = spnPaging.getSelectedItem().toString();
                if(page.equals(""+curPage)) return;
                loadingDialog.show();
                new SearchResultFragment.FetchSearchData().execute(query, page);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // Config button previous
        if(curPage > 1) btnPrev.setEnabled(true);
        else btnPrev.setEnabled(false);
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                new SearchResultFragment.FetchSearchData().execute(query, ""+(curPage-1));
            }
        });

        // Config button next
        if(curPage < jumlahHalaman) btnNext.setEnabled(true);
        else btnNext.setEnabled(false);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                new SearchResultFragment.FetchSearchData().execute(query, ""+(curPage+1));
            }
        });
    }

    private class FetchSearchData extends AsyncTask<String, Void, JSONObject>{
        String query, posisi, page;
        int batas = 5;

        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject ret = null;
            try{
                query = params[0];

                // Check mau request di page ke brp
                // Params[1] = page
                page = params[1];
                System.out.println("PAGE KE : " + page);
                posisi = getPosition(page, batas);

                HttpRequest getSearchResult = new HttpRequest(HttpRequest.baseURL + "question/search", "GET");
                getSearchResult.addParam("query", URLEncoder.encode(query, "UTF-8"));
                getSearchResult.addParam("start", posisi);
                getSearchResult.addParam("batas", ""+batas);

                String res = getSearchResult.execute();
                ret = new JSONObject(res);
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                return ret;
            }
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    JSONArray jsonArray = jsonObject.getJSONObject("payload").getJSONArray("data");
                    int len = jsonArray.length();
                    final ArrayList<Question> listQuestion = new ArrayList<>();
                    final String dataLen = jsonObject.getJSONObject("payload").getString("len");

                    TextView lblQuery = (TextView) parentActivity.findViewById(R.id.txtSearchQuery);
                    lblQuery.setText("Search result on \" " + query + "\"");

                    for(int i=0; i<len; i++){
                        JSONObject obj = jsonArray.getJSONObject(i);
                        Question q = new Question();
                        q.setId(obj.getString("id"));
                        q.setQuestion(obj.getString("title"));
                        q.setView(obj.getString("view"));
                        q.setTotalAnswer(obj.getString("total_answer"));
                        q.setTags(obj.getString("tags"));
                        q.setAskedby(obj.getJSONObject("userdata").getString("username"));
                        q.setImage(obj.getJSONObject("userdata").getString("profpic"));
                        q.setLastAction(obj.getString("action"));

                        listQuestion.add(q);
                    }

                    // do adapter shit here
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                QuestionAdapter adapter = new QuestionAdapter(parentActivity, listQuestion);
                                rv.setAdapter(adapter);
                                initPaging(query, Integer.parseInt(dataLen), Integer.parseInt(page), batas);
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                loadingDialog.hide();
            }
        }
    }
}
