package org.timdosen.bits.fragments.tabchild;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.timdosen.bits.R;
import org.timdosen.bits.data.adapter.QuestionAdapter;
import org.timdosen.bits.data.adapter.sqlite.ListQuestion;
import org.timdosen.bits.data.models.Question;

import java.util.ArrayList;

/**
 * Created by rynto14 on 4/23/2017.
 */

public class TrendingQuestionTab extends Fragment {
    private static Activity parentActivity;
    private static RecyclerView rv;
    private static QuestionAdapter adapter = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_question_list,container, false);
        parentActivity = getActivity();
        rv = (RecyclerView) rootView.findViewById(R.id.rvListQuestion);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        // cek apakah sudah ada isi atau belum

        updateListView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public static void updateListView (){
        //ListView lvwQuestionFeed = (ListView) parentActivity.findViewById(R.id.lvw_feed);
        ListQuestion lq = new ListQuestion(parentActivity.getApplicationContext());

        try {
            ArrayList<Question> titles = lq.getQuestionList("TREND");
            Log.d("DEBUG", titles==null ? "null" : "ngga");
            if(adapter != null && adapter.getItemCount() > 0){
                //adapter.removeAllItems();
                adapter.notifyDataSetChanged();
            }
            adapter = new QuestionAdapter(parentActivity, titles);
            /*lvwQuestionFeed.setAdapter(
                    new CustomListViewAdapter(parentActivity, titles)
            );*/
            rv.setAdapter(adapter);
            Log.d("DEBUG", titles==null ? "null" : "ngga");
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
