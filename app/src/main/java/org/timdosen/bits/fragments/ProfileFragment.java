package org.timdosen.bits.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Activity;

import android.support.v4.app.FragmentTabHost;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.activities.PostAnswerActivity;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.models.User;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;
import org.w3c.dom.Text;


/**
 * Created by vreynaldo on 4/24/17.
 */

public class ProfileFragment extends Fragment {
    private ProgressDialog loadingDialog = null;
    private Activity parentActivity = null;
    public static String TAG = "PROFILE";
    private String qid;
    private Button btnUpdate;

    public ProfileFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        parentActivity = getActivity();

        // hide button
        FloatingActionButton fab = (FloatingActionButton) parentActivity.findViewById(R.id.fab);
        fab.hide();

        // Show loading dialog
        loadingDialog = new ProgressDialog(parentActivity);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");
        loadingDialog.show();

        final String userid = this.getArguments().getString("username");
        final String mode = this.getArguments().getString("mode");
        MainActivity.currentState = MainActivity.PROFILE;

        new UpdateUserData().execute(userid);

        btnUpdate = (Button) rootView.findViewById(R.id.btnUpdate);
        if(mode.equals("self")) btnUpdate.setVisibility(View.VISIBLE);
        else btnUpdate.setVisibility(View.GONE);


        return rootView;
    }

    class UpdateUserData extends AsyncTask<String, Void, JSONObject>{
        private String username;
        @Override
        protected JSONObject doInBackground(String... params) {
            JSONObject jsonObject = null;
            try{
                username = params[0];
                HttpRequest userDetails = new HttpRequest(HttpRequest.baseURL + "user/info", "GET");
                userDetails.addParam("username", username);
                String res = userDetails.execute();
                jsonObject = new JSONObject(res);
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                return jsonObject;
            }
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    final JSONObject object = jsonObject.getJSONObject("payload");

                    // Image loader
                    final ImageLoader loader = new ImageLoader(parentActivity.getApplicationContext());

                    final ImageView imgProfil = (ImageView) parentActivity.findViewById(R.id.imgvProfile);
                    final TextView txtFullname = (TextView) parentActivity.findViewById(R.id.txtFullname);
                    final TextView txtEmail = (TextView) parentActivity.findViewById(R.id.txtEmail);
                    final TextView txtQasked = (TextView) parentActivity.findViewById(R.id.txtQasked);
                    final TextView txtQanswered = (TextView) parentActivity.findViewById(R.id.txtQanswered);

                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //String image = null;
                            try{
                                final String image = HttpRequest.baseURL + "user_img/"+ object.getString("img");
                                loader.DisplayImage(image, imgProfil);
                                txtFullname.setText(object.getString("fullname"));
                                txtEmail.setText(object.getString("email"));
                                txtQasked.setText(object.getString("questionAsked"));
                                txtQanswered.setText(object.getString("answerCount"));

                                btnUpdate.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        UpdateProfileFragment update = new UpdateProfileFragment();
                                        Bundle bundle = new Bundle();
                                        bundle.putString("fullname", txtFullname.getText().toString());
                                        bundle.putString("email", txtEmail.getText().toString());
                                        bundle.putString("img", image);
                                        update.setArguments(bundle);
                                        new MainActivity().changeFragment(update, true, UpdateProfileFragment.TAG);
                                    }
                                });
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
                else{
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                }
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                loadingDialog.hide();
            }
        }
    }
}
