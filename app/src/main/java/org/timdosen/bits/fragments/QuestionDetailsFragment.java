package org.timdosen.bits.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.lang.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.activities.PostAnswerActivity;
import org.timdosen.bits.data.adapter.AnswerAdapter;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.data.adapter.sqlite.ListQuestion;
import org.timdosen.bits.data.models.Answer;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;
import org.timdosen.bits.utils.Utils;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by rynto14 on 5/7/2017.
 */

public class QuestionDetailsFragment extends Fragment {
    private ProgressDialog loadingDialog = null;
    private Activity parentActivity = null;
    private static RecyclerView rv;
    public static String TAG = "QUESTION_DETAILS";
    private String qid;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_question_details, container, false);
        parentActivity = getActivity();
        rv = (RecyclerView) rootView.findViewById(R.id.rvListAnswer);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getContext()));

        // hide button
        FloatingActionButton fab = (FloatingActionButton) parentActivity.findViewById(R.id.fab);
        fab.hide();

        // Show loading dialog
        loadingDialog = new ProgressDialog(parentActivity);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");
        loadingDialog.show();

        final String qid = this.getArguments().getString("question_id");
        MainActivity.currentState = MainActivity.QUESTION_DETAIL;

        new FetchQuestion().execute(qid);

        Button btnReply = (Button) rootView.findViewById(R.id.btnPostReply);
        btnReply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(parentActivity, PostAnswerActivity.class);
                i.putExtra("id", qid);
                parentActivity.startActivity(i);
            }
        });

        return rootView;
    }

    private String getPosition(String page, int batas){
        if(page == null) return "0";
        return "" + ((Integer.parseInt(page)-1) * batas);
    }

    private void initPaging(final String questionID, int len, final int curPage, int batas){
        ImageButton btnNext = (ImageButton) parentActivity.findViewById(R.id.btnNext);
        ImageButton btnPrev = (ImageButton) parentActivity.findViewById(R.id.btnPrev);
        final Spinner spnPaging = (Spinner) parentActivity.findViewById(R.id.spnPaging);

        // Hitung jumlah halaman
        int jumlahHalaman = (int) Math.ceil((double) len / batas);

        // Generate jumlah halaman dan msukin ke spinner, masukin ke adapter, lalu tempel ke spinner
        List<String> spinnerArray =  new ArrayList<String>();
        for(int i=1; i<=jumlahHalaman; i++) spinnerArray.add(""+i);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(parentActivity, android.R.layout.simple_spinner_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnPaging.setAdapter(adapter);
        spnPaging.setSelection(adapter.getPosition(""+curPage));
        spnPaging.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String page = spnPaging.getSelectedItem().toString();
                if(page.equals(""+curPage)) return;
                loadingDialog.show();
                new FetchAnswerList().execute(questionID, page);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        // Config button previous
        if(curPage > 1) btnPrev.setEnabled(true);
        else btnPrev.setEnabled(false);
        btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                new FetchAnswerList().execute(questionID, ""+(curPage-1));
            }
        });

        // Config button next
        if(curPage < jumlahHalaman) btnNext.setEnabled(true);
        else btnNext.setEnabled(false);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                new FetchAnswerList().execute(questionID, ""+(curPage+1));
            }
        });
    }

    // Ambil detail pertanyaan dari server
    public class FetchQuestion extends AsyncTask<String, Void, JSONObject>{
        private String questionID;
        @Override
        protected JSONObject doInBackground(String... params) {
            questionID = params[0];
            HttpRequest getQuestionDetails = new HttpRequest(HttpRequest.baseURL + "question/id/" + questionID + "/true", "GET");
            try{
                String res = getQuestionDetails.execute();
                JSONObject getQuestObj = new JSONObject(res);
                return getQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    final JSONObject obj = jsonObject.getJSONObject("payload");

                    /*
                    *   Setting UI
                    * */
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                // Set profil image, load di cache memory
                                ImageView profilImage = (ImageView) parentActivity.findViewById(R.id.imgProfil);
                                ImageLoader imageLoader = new ImageLoader(parentActivity.getApplicationContext());
                                imageLoader.DisplayImage(HttpRequest.baseURL + "user_img/"+obj.getString("userpic"), profilImage);
                                profilImage.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // get username from sqlite database
                                        Account account = new Account(parentActivity.getApplicationContext());
                                        final String username = account.getUsername();

                                        ProfileFragment profileFragment = new ProfileFragment();
                                        Bundle bundle = new Bundle();
                                        try{
                                            bundle.putString("username", obj.getString("askedby"));
                                            if(username.equals(obj.getString("askedby"))) bundle.putString("mode", "self");
                                            else bundle.putString("mode", "else");
                                        } catch (Exception e){
                                            e.printStackTrace();
                                        }
                                        profileFragment.setArguments(bundle);
                                        new MainActivity().changeFragment(profileFragment, true, ProfileFragment.TAG);
                                    }
                                });

                                // Set yang lain-lain
                                TextView lblUsername = (TextView) parentActivity.findViewById(R.id.lblUsername);
                                lblUsername.setText(obj.getString("askedby"));

                                TextView lblAction = (TextView) parentActivity.findViewById(R.id.lblAction);
                                lblAction.setText(obj.getString("last_action"));

                                TextView lblTitle = (TextView) parentActivity.findViewById(R.id.lblTitle);
                                lblTitle.setText(obj.getString("title"));

                                String s = StringEscapeUtils.unescapeHtml(obj.getString("content"));
                                WebView wbvContent = (WebView) parentActivity.findViewById(R.id.wbvQuestionContent);
                                String html = "<head><style>pre{max-width:5px; max-height:100px;}</style></head><body>" + s + "</body>";
                                wbvContent.loadDataWithBaseURL("", html, "text/html", "UTF-8", "");
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                    // Load answerlist
                    new FetchAnswerList().execute(questionID, "1");
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public class FetchAnswerList extends AsyncTask<String, Void, JSONObject>{
        String posisi, page, qID;
        int batas = 5;

        @Override
        protected JSONObject doInBackground(String... params) {
            qID = params[0];

            // Check mau request di page ke brp
            // Params[1] = page
            page = params[1];
            System.out.println("PAGE KE : " + page);
            posisi = getPosition(page, batas);

            HttpRequest getAnswerList = new HttpRequest(HttpRequest.baseURL + "answer/fromquestion", "POST");
            getAnswerList.addParam("qid", qID);
            getAnswerList.addParam("start", posisi);
            getAnswerList.addParam("batas", ""+batas);
            try{
                String res = getAnswerList.execute();
                JSONObject getQuestObj = new JSONObject(res);
                return getQuestObj;
            }
            catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(final JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try{
                boolean isSuccess = jsonObject.getBoolean("success");
                if(isSuccess){
                    JSONArray jsonArray = jsonObject.getJSONObject("payload").getJSONArray("answerList");
                    int len = jsonArray.length();
                    final ArrayList<Answer> listAnswer = new ArrayList<>();
                    final String dataLen = jsonObject.getJSONObject("payload").getString("len");

                    TextView lblNumAnswer = (TextView) parentActivity.findViewById(R.id.totalAnswer);
                    lblNumAnswer.setText(dataLen + " answers");

                    for(int i=0; i<len; i++){
                        JSONObject obj = jsonArray.getJSONObject(i);
                        Answer a = new Answer(
                                obj.getString("answer"),
                                Utils.formatDate(obj.getString("date")),
                                obj.getString("answeredby"),
                                obj.getString("userpic"),
                                obj.getString("id_answer")
                        );
                        listAnswer.add(a);
                    }
                    // do adapter shit here
                    //ListView lvwQuestionFeed = (ListView) parentActivity.findViewById(R.id.lvw_feed);
                    //ListQuestion lq = new ListQuestion(parentActivity.getApplicationContext());

                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                AnswerAdapter adapter = new AnswerAdapter(parentActivity, listAnswer);
                                rv.setAdapter(adapter);
                                initPaging(qID, Integer.parseInt(dataLen), Integer.parseInt(page), batas);
                            } catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });
                    loadingDialog.dismiss();
                }
                else {
                    parentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                loadingDialog.dismiss();
                                Toast.makeText(parentActivity.getApplicationContext(), jsonObject.getString("errMsg"), Toast.LENGTH_SHORT).show();
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
