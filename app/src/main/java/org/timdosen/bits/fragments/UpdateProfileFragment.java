package org.timdosen.bits.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import org.json.JSONObject;
import org.timdosen.bits.R;
import org.timdosen.bits.activities.LoginActivity;
import org.timdosen.bits.activities.MainActivity;
import org.timdosen.bits.activities.RegisterActivity;
import org.timdosen.bits.activities.SplashScreenActivity;
import org.timdosen.bits.data.adapter.sqlite.Account;
import org.timdosen.bits.utils.HttpRequest;
import org.timdosen.bits.utils.ImageLoader;

import java.io.ByteArrayOutputStream;

/**
 * Created by rynto14 on 5/18/2017.
 */

public class UpdateProfileFragment extends Fragment {
    private ProgressDialog loadingDialog = null;
    private Activity parentActivity = null;
    public static String TAG = "PROFILE";
    private String b64 = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_editprofile, container, false);
        parentActivity = getActivity();

        // hide button
        FloatingActionButton fab = (FloatingActionButton) parentActivity.findViewById(R.id.fab);
        fab.hide();

        // Show loading dialog
        loadingDialog = new ProgressDialog(parentActivity);
        loadingDialog.setIndeterminate(true);
        loadingDialog.setCanceledOnTouchOutside(false);
        loadingDialog.setMessage("Loading data...");

        final String fullname = this.getArguments().getString("fullname");
        final String email = this.getArguments().getString("email");
        final String img = this.getArguments().getString("img");
        MainActivity.currentState = MainActivity.UPDATE;

        //new ProfileFragment.UpdateUserData().execute(userid);

        final EditText txtFullname = (EditText) rootView.findViewById(R.id.txtFullname);
        txtFullname.setText(fullname);
        final EditText txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        txtEmail.setText(email);

        ImageView imgProfile = (ImageView) rootView.findViewById(R.id.imgvProfile);
        ImageLoader loader = new ImageLoader(parentActivity.getApplicationContext());
        loader.DisplayImage(img, imgProfile);
        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, 123);
            }
        });

        Button btnUpdateProfile = (Button) rootView.findViewById(R.id.btnUpdate);
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                final Account user = new Account(getActivity().getApplicationContext());
                final String uname = user.getUsername();

                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        try{
                            HttpRequest httpRequest = new HttpRequest(HttpRequest.baseURL+"user/update/"+uname, "POST");
                            httpRequest.addParam("email", txtEmail.getText().toString());;
                            httpRequest.addParam("full_name", txtFullname.getText().toString());;

                            String res = httpRequest.execute();

                            JSONObject obj = new JSONObject(res);
                            if(obj.getBoolean("success")){
                                user.setProfile(txtFullname.getText().toString(), txtEmail.getText().toString());

                            }
                            else{
                                // Data nya gaada, direct ke halaman regis

                            }
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                };

                thread.start();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 123){
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = parentActivity.getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            Bitmap bm = BitmapFactory.decodeFile(picturePath);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            bm.recycle();
            bm = null;
            byte[] b = baos.toByteArray();
            b64= Base64.encodeToString(b, Base64.DEFAULT);
        }
    }
}
